#import "CredflowWebViewPlugin.h"
#if __has_include(<credflow_web_view/credflow_web_view-Swift.h>)
#import <credflow_web_view/credflow_web_view-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "credflow_web_view-Swift.h"
#endif

@implementation CredflowWebViewPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftCredflowWebViewPlugin registerWithRegistrar:registrar];
}
@end
